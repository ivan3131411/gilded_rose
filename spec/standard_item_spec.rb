require_relative '../src/item'
require_relative '../src/glided_rose'


describe 'Standard item' do

  let(:standard_item_name) {'Standard item'}
  let(:sell_in) {10}
  let(:sell_in_today) {0}
  let(:quality) {20}
  let(:minimum_quality) {0}
  let(:item) {Item.new(standard_item_name,sell_in,quality)}

  it 'has a starting sell-in value' do
    expect(item.sell_in).to eq(10)
  end

  it 'has a starting quality value' do
    expect(item.quality).to eq(20)
  end

  it 'at the end of each day, its sell-in value descends' do
    items = [item]
    store = GildedRose.new(items)

    store.update_quality

    expect(item.sell_in).to eq(sell_in - 1)
  end

  it 'at the end of each day, its quality descends' do
    items = [item]
    store = GildedRose.new(items)

    store.update_quality

    expect(item.quality).to eq(quality - 1)
  end

  it 'when its sell-in date has passed, quality degrades twice as fast' do
    item.sell_in = sell_in_today
    items = [item]
    store = GildedRose.new(items)

    store.update_quality

    expect(item.quality).to eq(quality - 2)
  end

  it 'its quality can never be negative' do
    item.quality = minimum_quality
    items = [item]
    store = GildedRose.new(items)

    store.update_quality

    expect(item.quality).to eq(minimum_quality)
  end

end