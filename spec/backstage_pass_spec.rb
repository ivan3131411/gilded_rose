require_relative '../src/item'
require_relative '../src/glided_rose'

describe 'Backstage pass' do

  let (:backstage_pass_item_name) {'Backstage passes to a TAFKAL80ETC concert'}
  let (:quality) {20}
  let (:default_sell_in_value) {20}
  let (:backstage_pass) {Item.new(backstage_pass_item_name, default_sell_in_value, quality)}

  context 'Concert date is far away' do
    it 'increases quality by one' do
      far_away_date = 15
      backstage_pass.sell_in = far_away_date
      items = [backstage_pass]
      inventory = GildedRose.new(items)

      inventory.update_quality

      expect(backstage_pass.quality).to eq(quality + 1)
    end
  end

  context 'Concert date is coming closer' do
    it 'increases quality by two' do
      coming_closer_date = 9
      backstage_pass.sell_in = coming_closer_date
      items = [backstage_pass]
      inventory = GildedRose.new(items)

      inventory.update_quality

      expect(backstage_pass.quality).to eq(quality + 2)
    end
  end

  context 'Concert date is close' do
    it 'increases quality by three' do
      close_date = 5
      backstage_pass.sell_in = close_date
      items = [backstage_pass]
      inventory = GildedRose.new(items)

      inventory.update_quality

      expect(backstage_pass.quality).to eq(quality + 3)
    end
  end

  context 'Concert date has expired' do
    it 'decreases quality to zero' do
      expired_date = 0
      backstage_pass.sell_in = expired_date
      items = [backstage_pass]
      inventory = GildedRose.new(items)

      inventory.update_quality

      expect(backstage_pass.quality).to eq(0)
    end
  end
end
