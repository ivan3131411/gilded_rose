require_relative '../src/item'
require_relative '../src/glided_rose'

describe 'Aged Brie' do
  let(:aged_brie) {'Aged Brie'}
  let(:sell_in) {8}
  let(:quality) {49}
  let(:max_quality) {50}
  let (:item) {Item.new(aged_brie,sell_in,quality)}

  it 'increases quality by one when the sell-in is decreased' do
    products = [item]

    inventory = GildedRose.new(products)
    inventory.update_quality

    expect(item.quality).to eq(max_quality)
  end

  it 'quality can not exceed the max quality' do
    item.quality = max_quality
    products = [item]

    inventory = GildedRose.new(products)
    inventory.update_quality

    expect(item.quality).to eq(max_quality)
  end

  it 'when the sell-in has expired the quality increases by two' do
    item.sell_in = 0
    item.quality = 48
    products = [item]

    inventory = GildedRose.new(products)
    inventory.update_quality

    expect(item.quality).to eq(max_quality)
  end
end
