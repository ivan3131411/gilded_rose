require_relative '../src/item'
require_relative '../src/glided_rose'

describe GildedRose do
  let(:item_name) { 'Sulfuras, Hand of Ragnaros' }
  let(:sulfuras_sell_in) { 1 }
  let(:sulfuras_quality) { 80 }

  it 'dont devaluates Sulfuras over time' do
    sulfuras = Item.new(item_name, sulfuras_sell_in, sulfuras_quality)
    items = [sulfuras]
    inventory = GildedRose.new(items)

    inventory.update_quality

    expect(sulfuras.quality).to eq(sulfuras_quality)
    expect(sulfuras.sell_in).to eq(sulfuras_sell_in)
  end
end
