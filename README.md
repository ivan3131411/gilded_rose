## Objetos

Inventario
  Producto
  cantidad

Categoría

Producto = Item
  Calidad
  sell-in value


## Reglas de negocio

- All items have a sell-in value which denotes the number of days we have to sell the item
- All items have a quality value which denotes how valuable the item is
- At the end of each day our system lowers both values for every item
- Once the sell by date has passed, quality degrades twice as fast
- The quality of an item is never negative
- The quality of an item is never more than 50

## Reglas de productos

- Normal item, le afecta tó.
- "Aged Brie" actually increases in quality the older it gets
- "Sulfuras", being a legendary item, never has to be sold or decreases in quality, its quality is 80 and it never alters
- "Backstage passes", like aged brie, increases in quality as it's sell-in value approaches; quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less but quality drops to 0 after the concert

## Tasks

- Prepare environment (https://github.com/amckinnell/Gilded-Rose-Ruby/commit/0ab533b97461b7eacc9adc1ad373ad267e3582f7)
- Test standard item
- Test Aged Brie
- Test Sulfuras
- Test Backstage


# spec/gilded_rose_spec.rb
describe GildedRose do
  it 'works' do
    standard_item_name = 'Standard Item'
    item = Item.new(standard_item_name, sell_in=10, quality=20)
    items = [item]
    store = GildedRose.new(items)

    store.update_quality

    expect(item.name).to eq(standard_item_name)
    expect(item.quality).to eq(19)
    expect(item.sell_in).to eq(9)
  end
end

Build project:
  docker-compose up --build
  docker-compose run --rm kata bash
  bundle install
  rspec
