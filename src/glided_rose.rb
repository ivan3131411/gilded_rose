require_relative 'item'
require_relative 'item_updater'

class GildedRose
  def initialize(items)
    @items = items
  end

  def update_quality()
    @items.each do |raw_item|
      item = ItemUpdater.new(raw_item)

      if item.is?("Sulfuras, Hand of Ragnaros")
        break
      end

      if item.is?("Aged Brie")
        item.increase_quality_by_one
        item.update_sell_in
        if item.sell_in_date_has_passed?
          item.increase_quality_by_one
        end
        break
      end

      if item.is?("Backstage passes to a TAFKAL80ETC concert")
        item.increase_quality_by_one
        if item.ten_days_or_less_for_the_concert?
          item.increase_quality_by_one
        end
        if item.five_days_or_less_for_the_concert?
          item.increase_quality_by_one
        end
        item.update_sell_in
        if item.sell_in_date_has_passed?
          item.decrease_quality_to_zero
        end
        break
      end

      item.decrease_quality_by_one
      item.update_sell_in
      if item.sell_in_date_has_passed?
        item.decrease_quality_by_one
      end
    end
  end
end