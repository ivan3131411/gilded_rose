class ItemUpdater

  MINIMUM_QUALITY = 0
  MAXIMUM_QUALITY = 50

  def initialize(item)
    @item = item
  end

  def is?(name)
    @item.name.eql? name
  end

  def decrease_quality_by_one
    return unless is_above_min_quality?
    @item.quality -= 1
  end

  def decrease_quality_to_zero
    @item.quality = 0
  end

  def increase_quality_by_one
    return unless is_below_max_quality?
    @item.quality += 1
  end

  def ten_days_or_less_for_the_concert?
    @item.sell_in <= 10
  end

  def five_days_or_less_for_the_concert?
    @item.sell_in <= 5
  end

  def sell_in_date_has_passed?
    @item.sell_in < 0
  end

  def update_sell_in
    @item.sell_in = @item.sell_in - 1
  end

  private

  def is_above_min_quality?
    @item.quality > MINIMUM_QUALITY
  end

  def is_below_max_quality?
    @item.quality < MAXIMUM_QUALITY
  end
end